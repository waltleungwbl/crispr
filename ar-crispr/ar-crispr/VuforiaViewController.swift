//
//  VuforiaViewController.swift
//  ar-crispr-sample
//
//  Created by Felipe Campos on 4/18/18.
//  Copyright © 2018 VRAB. All rights reserved.
//

import UIKit
import AVFoundation

class VuforiaViewController: UIViewController {
    
    // MARK: UI & Unity Setup
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initUnity()
    }
    
    /** Interfaces with App Delegate to initialize Unity & Vuforia. */
    func initUnity() {
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            appDelegate.startUnity()
            
            let unityView = UnityGetGLView()
            self.view.addSubview(unityView!)
            
            // NotificationCenter.default.addObserver(self, selector: #selector(handleButtonPress), name: NSNotification.Name("UnityToggleRotation"), object: nil)
            unityView!.translatesAutoresizingMaskIntoConstraints = false
        }
    }
    
    /** Sets the navigation bar to be translucent so it doesn't block the Unity GLView. */
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
    }
    
    /** Stops Unity before we go back to the main screen. */
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            appDelegate.stopUnity()
        }
    }
    
    // MARK: Methods
    
    /** NOTE: have buttons that highlight crRNA, target DNA, gRNA, targetRNA, iron-sulfur cluster, ATP, etc.
     for each Cas protein. First worry about keeping track of which protein is currently being rendered, shouldn't
     be that difficult.
    */
    
    /*
    @objc func handleUnityToggleRotation(_ n: NSNotification) {
        if let isOn = n.userInfo?["isOn"] as? NSNumber {
            rotateSwitch.isOn = isOn.boolValue
        }
    }
    
    @IBAction func handleSwitchValueChanged(sender: UISwitch) {
        UnityPostMessage("NATIVE_BRIDGE", "RotateCube", sender.isOn ? "start" : "stop")
    } */
}
