//
//  ViewController.swift
//  ar-crispr-sample
//
//  Created by Felipe Campos on 4/18/18.
//  Copyright © 2018 VRAB. All rights reserved.
//

/** NOTE: Make transition animation to Vuforia Controller more natural. */

import UIKit
import AVFoundation

class ViewController: UIViewController {

    // MARK: Properties & Views
    
    /** UIView for our video capture session. */
    @IBOutlet var cameraView: UIView!
    
    /** Some buttons. */
    @IBOutlet var aboutButton: UIButton!
    @IBOutlet var cameraButton: UIButton!
    @IBOutlet var crisprCardsButton: UIButton!
    
    /** Capture session properties. */
    var captureSession: AVCaptureSession?
    var cameraPreviewLayer: AVCaptureVideoPreviewLayer?
    var captureInput: AnyObject?
    
    // MARK: UI Setup
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initCaptureSession()
        
        aboutButton.makeBezierPath()
        cameraButton.makeBezierPath()
        crisprCardsButton.makeBezierPath()
        
        self.view.bringSubview(toFront: aboutButton)
        self.view.bringSubview(toFront: cameraButton)
        self.view.bringSubview(toFront: crisprCardsButton)
    }
    
    /** Hides the navigation bar and starts the capture session if we've previously stopped it. */
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.isNavigationBarHidden = true
        if captureSession != nil {
            captureSession?.startRunning()
        }
    }
    
    /** Stops our capture session before we leave the view. */
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.navigationController?.isNavigationBarHidden = false
        captureSession?.stopRunning()
    }
    
    /** Creates an AVCaptureSession for showing video input on our main view controller. */
    func initCaptureSession() {
        let videoCaptureDevice = AVCaptureDevice.default(for: AVMediaType.video)
        do {
            captureInput = try AVCaptureDeviceInput(device: videoCaptureDevice!)
        } catch {
            NSLog("Failed to get video input.")
        }
        
        captureSession = AVCaptureSession()
        captureSession?.addInput(captureInput as! AVCaptureInput)
        
        cameraPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession!)
        cameraPreviewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        cameraPreviewLayer?.frame = self.view.layer.bounds
        
        self.view.layer.addSublayer(cameraPreviewLayer!)
        
        captureSession?.startRunning()
    }

    // MARK: Actions
    
    /** Displays some of our protein card targets to the user. */
    @IBAction func displayCards(_ sender: UIButton) {
        let url = Bundle.main.url(forResource: "proteinCardTargets", withExtension: "pdf")
        let documentInteractionController = UIDocumentInteractionController.init(url: url!)

        documentInteractionController.delegate = self
        documentInteractionController.presentPreview(animated: true)
    }
    
    /** Takes user to Innovative Genomics Institute home page. */
    @IBAction func aboutPressed(_ sender: UIButton) {
        guard let url = URL(string: "https://innovativegenomics.org/") else {
            return
        }
        
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
    
    // MARK: Navigation
    
    /** Stops running our current capture session and segues
     to the Vuforia view controller where we will initialize
     our AR camera through Unity. */
    @IBAction func initializeARCamera(_ sender: UIButton) {
        performSegue(withIdentifier: "unityInit", sender: nil)
    }
}

// MARK: - UIDocumentInteractionControllerDelegate
extension ViewController: UIDocumentInteractionControllerDelegate {
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
        return self
    }
}

// MARK: - UIButton View Extension
extension UIButton {
    /** Gives button edges a more satisfying look. */
    func makeBezierPath() {
        let cornerRadius: CGFloat = 8
        let shadowOffsetWidth: Int = 3
        let shadowOffsetHeight: Int = 5
        let shadowColor: UIColor? = UIColor.black
        let shadowOpacity: Float = 0.5
        
        layer.cornerRadius = cornerRadius
        let shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius)
        
        layer.masksToBounds = false
        layer.shadowColor = shadowColor?.cgColor
        layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight)
        layer.shadowOpacity = shadowOpacity
        layer.shadowPath = shadowPath.cgPath
    }
}

